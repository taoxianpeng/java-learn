package com.txp.exercise.stream;

import java.io.*;

public class CopyFile{
    public static void main(String[] args) throws IOException{
        // String a = args[0];
        String a = "/home/txp/文档/JavaEx/Excesize/src/com/txp/exercise/stream/CopyFile.java";
        String b = "";
        System.out.println(writeFile(a));
    }
    static String writeFile(String filename) throws IOException{
        String s;
        
        try(InputStream input=new FileInputStream(filename)){
            int n;
            StringBuilder buffer = new StringBuilder();
            while ((n=input.read())!=-1) {
                buffer.append((char) n );
            }
            s = buffer.toString();
        }
        return s;

    }
}